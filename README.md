Stowpkg: ports system based on GNU Stow
=======================================

A portable, non-conflicting Unix package manager built atop of [GNU
Stow](https://www.gnu.org/software/stow/).

Features
--------

* Non-conflicting with any other package management in use (distro-specific, npm, pip, etc).
* Multi-target support: you can keep may isolated and independent software trees.
* Multi-distro support: works for any GNU/Linux and possible any BSD-variant.
* Repository signature verification using GnuPG and signed git commits.
* Small enough so everybody can learn how a package manager works.

Installation
------------

Simply clone it and add to your `$PATH`:

    git clone https://git.fluxo.info/stowpkg

You can also verify the latest commit's OpenPGP signature:

    /usr/bin/git -C stowpkg verify-commit HEAD

You'll also need some dependencies. On a Debian-like system those can be
installed using

    sudo apt install stow build-essential

Usage
-----

    stowpkg install   <package>           # install   packages from source
    stowpkg switch    <package> <version> # switch a  package to another available version
    stowpkg remove    <package>           # remove    packages
    stowpkg reinstall <package>           # reinstall packages
    stowpkg build                         # simple    package  builder (./configure && make && make install)
    stowpkg search                        # list      packages available on ports
    stowpkg list                          # list      packages installed
    stowpkg sources                       # list      program  sources available
    stowpkg binaries                      # list      program  binaries available
    stowpkg purge     <package>           # purge     packages
    stowpkg upgrade   <package>           # upgrade   packages
    stowpkg update                        # update    ports    repository

Running installed applications
------------------------------

Make sure to include stowpkg package tree into your `PATH`, `MANPATH`, `LD_LIBRARY_PATH`, etc.

Configuration
-------------

Use `~/.config/stowpkg` do put any default parameters. Current supported params include

* `BASE`: defaults to `<stowpkg-repo>/tree/<architecture>`, but can be
  configured to anything like `/usr/local` with corresponding packages
  installed at `/usr/local/stow`.

Filesystem layout
-----------------

Current ports tree is distributed directly along with `stowpkg` source repository.
A binary repository has the following layout starting from `$BASE` (like `/usr/local`):

    {bin,lib,man,share,etc,src}: usual Unix paths
    stow
        package_name-VERSION

What it does
------------

If you want to do it manually:

    sudo apt install <luakit-depencies>
    git clone https://github.com/luakit/luakit
    cd luakit
    make PREFIX=/usr/local/stow/luakit
    sudo make PREFIX=/usr/local/stow/luakit install
    cd /usr/local/stow/
    sudo stow luakit

Further development
-------------------

* Automatic application patching.
* Source code verification using multiple methods (commit hash, checksums, OpenPGP).
* Port releases using git branches and tags, allowing to swtich the current
  installed applications' versions by doing a `git checkout` and a `stowpkg
  switch`: that way you could easily switch all packages to the versions shipped
  in a given branch.

Other commands can easily be implemented:

    stowpkg upstream <package> # check packages versions at upstream

Also, per-architecture binary package support might be implemented using
git-annex and GnuPG signatures, giving us commands like

    stowpkg pack  <package> # pack an installed package
    stowpkg fetch <package> # fetch package from the repo

References
----------

* [GNU Stow Docs](http://www.gnu.org/software/stow/manual/html_node/index.html).
* [Using GNU Stow to manage source installs](http://blog.danieroux.com/2005/08/07/using-gnu-stow-to-manage-source-installs/).
* [Get rid of stowaway packages with GNU Stow](https://www.linux.com/news/get-rid-stowaway-packages-gnu-stow).
* [GoboLinux](https://gobolinux.org/).
